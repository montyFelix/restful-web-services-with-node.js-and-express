var should = require('should'),
    sinon = require('sinon');

describe('Book Controller Tests', function() {
  describe('Post Tests', function() {
    it('should not allow an empty title on post', function() {
      var Book = function(book) {this.save = function(){};};
      var req = {
        body: {
          title: "Foundation",
          author: 'Isaac Asimov'
        }
      };
      var res = {
        status: sinon.spy(),
        send: sinon.spy()
      };

      var bookController = require('../controllers/bookController')(Book);

      bookController.post(req, res);
      res.status.calledWith(400).should.equal(true, 'bad status ' + res.status.args[0][0]);
      res.send.calledWith('Fill all required fields').should.equal(true);
    });
  });
});
